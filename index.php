<!DOCTYPE html>
<html  lang="en" ng-app="onestopsolutionsApp">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="Natasha Nodine Wyatt">
	<title>Vilcabamba Hotel Caba&ntilde;as R&iacute;o Yambala</title>
	
	<link rel="stylesheet" href="css/bootstrap.min.css" >
	<link href="css/main.css" rel="stylesheet">

</head>

<body>
	
	
	
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Navbar</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Features</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Pricing</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Dropdown link
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
    </ul>
  </div>
</nav>
	
	
	
	
	
	
	
	<?php include ('vilcabamba-ecuador/carousel.php'); ?>
	<div class="container">
	
	</div>
	<footer>
	
		<div class="row">
	<div class="col-md-4 social_networks">
	</div>
	
	<div class="col-md-4 footer_logo">
		<img src="images/one-stop-solutions-logo.png" alt="one-stop-solutions web master">
	</div>
	<div class="col-md-4 ">
	</div>
	</div>
		<div class="row">
			<div class="col-md-12 copy_right">
				<a href="http://onestopsolutionswebmasters.com/" target="blank"><small>Copyright &copy; <?php echo date("Y"); ?> One Stop Solutions All Rights Reserved</a>
			</div>
		</div>
	
	
	
	
	</footer>
	

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="node_modules/popper.js/umd/popper.min.js"></script>
	<script src="js/bootstrap.min.js" ></script>
	
	
</body>

</html>