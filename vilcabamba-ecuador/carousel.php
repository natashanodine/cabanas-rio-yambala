
<div id="vilcabamabaCarousel" class="carousel slide" data-ride="carousel">

    <div class="carousel-inner" role="listbox">
		<div class="item active">
			<img src="../images-vilcabamba-ecuador/vilcabamba-loja-ecuador/vilcabamba-loja-equador.jpg" alt="">
			<div class="carousel-caption">
			  <h2>Vilcabamba</h2>
			</div>
		</div>

		<div class="item">
			<img src="../images-vilcabamba-ecuador/vilcabamba-loja-ecuador/vilcabamba-loja-equador-1.jpg" alt="">
			<div class="carousel-caption">
			  <h2>Vilcabamba</h2>
			</div>
		</div>
		<div class="item">
			<img src="../images-vilcabamba-ecuador/vilcabamba-loja-ecuador/vilcabamba-loja-ecuador-3.jpg" alt="">
			<div class="carousel-caption">
			  <h2>Vilcabamba</h2>
			</div>
		</div>
		<div class="item">
			<img src="../images-vilcabamba-ecuador/vilcabamba-loja-ecuador/vilcabamba-loja-equador-2.jpg" alt="">
			<div class="carousel-caption">
			  <h2>Vilcabamba</h2>
			</div>
		</div>
		<div class="item">
			<img src="../images-vilcabamba-ecuador/vilcabamba-loja-ecuador/vilcabamba-loja-ecuador-2.jpg" alt="">
			<div class="carousel-caption">
			  <h2>Vilcabamba</h2>
			</div>
		</div>
		<div class="item">
			<img src="../images-vilcabamba-ecuador/vilcabamba-loja-ecuador/vilcabamba-loja-ecuador-1.jpg" alt="">
			<div class="carousel-caption">
			  <h2>Vilcabamba</h2>
			</div>
		</div>

	
    </div>

    <a class="left carousel-control" href="#vilcabamabaCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#vilcabamabaCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>